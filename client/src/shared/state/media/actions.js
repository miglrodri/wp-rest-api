import buildUrl from 'build-url';
// import deepcompact from 'deep-compact';
import axios from 'axios';

import { actions } from './reducer';
import { adaptMedia } from '../../helpers/adapters';

const RESOURCE_URL = '/wp-json/wp/v2/media';

export const get = (page = 1, slug = '') => (dispatch, getState) => {
    dispatch(actions.fetchStart());

    const baseUrl = getState().page.config.cms;
    const url = buildUrl(baseUrl, {
        path: RESOURCE_URL,
        // hash: 'contact',
        // queryParams: deepcompact({
        //     page,
        //     slug,
        // }),
    });

    axios.get(url)
        .then(response => response.data)
        .then(data => {
            dispatch(actions.fetchSuccess(adaptMedia(data)));
        })
        .catch(error => {
            // handle error
            console.error(error);
            dispatch(actions.fetchFail(error.toString()));
        });
};
