## run composer commands

docker image: https://hub.docker.com/_/composer

unix: `docker run --rm -v $(pwd):/app composer install`

powershell: `docker run --rm -v ${pwd}:/app composer install`

## dump sql data

`docker exec wp-app_database_1 bash -c "mysqldump --user=homestead --password=secret homestead > /docker-entrypoint-initdb.d/data.sql"`

<!-- `docker exec wp-app_database_1 sh -c 'exec mysqldump --all-databases -u"homestead" -p"secret"' > ${pwd}/sql/all-databases.sql` -->

## restore sql

`docker exec wp-base-app_database_1 bash -c "mysql --user=homestead --password=secret homestead < /sql/data111.sql"`

`docker exec wp-app_database_1 bash -c "mysql --user=homestead --password=secret homestead < /docker-entrypoint-initdb.d/all123.sql"`

## jump into container

`docker exec -i -t container_name /bin/bash`

## less with wp

`https://www.noeltock.com/web-design/wordpress/using-less-with-wordpress/`


### troubleshooting


## links

mocks: `https://www.dropbox.com/sh/uetg3gt4ixgf4pr/AAAdbWAOWawNonCBbViDh0Dva?dl=0&fbclid=IwAR2JjOcCyQoKj55I2vNxFcAkeQail6BUf5-4VJeSOePJr-wnO6kDD6lPym8`
loop posts: `https://wordpress.stackexchange.com/questions/37155/simply-loop-through-posts`

staging:
dbname. sinapser_v200
user. sinapser_wpuser
pass. jHrkcQIkq0m2

sinapserh
G78Seycwr1Wa

wp-rest-api
adadmin
2a6Lkb!$q!hS
migl.rodri...

https://www.codeinwp.com/blog/wordpress-rest-api/
http://localhost:3000/wp-json/wp/v2
http://localhost:3000/wp-json/wp/v2/pages
http://localhost:8301/wp-json/wp/v2/posts
https://www.youtube.com/watch?v=PhwPpEC7YG4
http://localhost:3000/wp-admin/plugin-install.php?s=form&tab=search&type=term
https://www.youtube.com/watch?v=eBn4Qjp4F3w

Setting Up a Redux Project With Create-React-App
https://medium.com/backticks-tildes/setting-up-a-redux-project-with-create-react-app-e363ab2329b8

How to create a modern web app using WordPress and React
https://www.freecodecamp.org/news/wordpress-react-how-to-create-a-modern-web-app-using-wordpress-ef6cc6be0cd0/

Deploy / Host your React App with cPanel in Under 5 Minutes
https://dev.to/crishanks/deploy-host-your-react-app-with-cpanel-in-under-5-minutes-4mf6

How to fix maximum upload and php memory limit issues in WordPress ?
https://docs.presscustomizr.com/article/171-fixing-maximum-upload-and-php-memory-limit-issues

https://reacttraining.com/react-router/web/example/query-parameters

https://www.elharony.com/all-in-one-wp-migration-import-stuck-problem/
https://create-react-app.dev/docs/advanced-configuration/
https://serverless-stack.com/chapters/code-splitting-in-create-react-app.html
https://www.robinwieruch.de/react-function-component

Access Control Headers For The WordPress REST API
https://joshpress.net/access-control-headers-for-the-wordpress-rest-api/

http://example.com/wp-json/wp/v2/posts?categories=20,30

ver se o form está disponível por rest api

# prod
ihcoimbr_wordpress
user:ihcoimbr_wp
p:wGFfycyuB.Tt

## now
https://zeit.co/docs/now-cli/ now --name foo now --no-clipboard
